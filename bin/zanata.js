#! /usr/bin/env node

const program = require('../lib/util/args-parser.js');
const cli = require('../lib/util/cli.js');
const pkg = require('../package.json');

program
  .version(pkg.version)
  .overrideAlias(true)
  .main(function(self) {
    self.help();
  })
  .option({name: 'url',
	   alias: 'U',
	   desc: 'Zanata server URL to connect',
	   argument: '<URL>',
	   callback: function(self, obj, cb) {
	     if (obj.value.match(/^https?:\/\//))
	       cb(null);
	     else
	       cb('Invalid URL');
	   }})
  .option({name: 'user',
	   alias: 'u',
	   desc: 'User account on Zanata server',
	   argument: '<USER>'})
  .option({name: 'api-key',
	   alias: 'K',
	   desc: 'API key to access on Zanata server',
	   argument: '<KEY>'})
  .option({name: 'disable-ssl-cert',
           desc: 'Where verification of SSL certificates should be disabled',
           callback: function(self, obj, cb) {
             process.env['NODE_TLS_REJECT_UNAUTHORIZED'] = '0';
             cb(null);
           }});

cli(function(err, cmds) {
  if (err) {
    console.log(err);
    process.exit(1);
  }
  Object.keys(cmds).forEach(function(key) {
    cmd = cmds[key];
    program
      .command(cmd)
      .usage(cmd.usage)
      .description(cmd.desc)
      .example(cmd.examples)
      .options(cmd.options)
      .main(cmd.main)
      .commands(cmd.commands)
      .end()
  });
  program.parse(process.argv);
});
