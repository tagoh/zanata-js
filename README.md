# zanata-js [![NPM version](https://badge.fury.io/js/zanata-js.png)](http://badge.fury.io/js/zanata-js)

zanata-js is a Client library for Zanata in JavaScript.
Zanata is a web-based system for translators, content creators and developers to manage localization projects. this library provides APIs to access to zanata from JavaScript-based projects.

## Installation ##

```
#!shell

% npm install zanata-js
```

## Getting Started with CLI

### Common Options

#### -U, --url=`<URL>`

Specify the URL to the Zanata server where you want to connect. this can be omitted if you download zanata.xml from the Zanata.

#### -u, --user=`<USER>`

Specify your account name on the Zanata. this can be omitted if you add your account name into zanata.ini according to the [instructions](https://github.com/zanata/zanata-p
latform/blob/master/docs/client/configuration.md).

#### -K, --api-key=`<KEY>`

Specify your API key to access the Zanata API. this can be omitted if you add your API key into zanata.ini.

#### --disable-ssl-cert

If you see any trouble to access to the Zanata via SSL, you can use this option as a workaround. though this isn't recommended.

### Project management

#### Common Options for projects

##### -C, --config=`<FILE>`

Specify the place to zanata.xml which you want to read for the project. the default behavior is to read it on the current directory though, if you want to use the different one, this may helps.

#### How to get all of projects available on the Zanata

##### Command

```shell
$ zanata-js project list
```

##### Options

###### -c, --column=<column name,...>

Columns you want to show is customizable with this option. the available column name is, `id`, `name`, `defaultType`, `status`, and `all`. `id`, `name` and `status` is displayed by default.

#### How to create a project in the Zanata

##### Command

```shell
$ zanata-js project create [options] `<project id>`
```

Create a project with `project id`.

##### Options

###### -d, --description=`<DESCRIPTION>`

The project description. this is optional.

###### -p, --project=`<NAME>`

The project name. this is required to create.

###### -t --project-type=`<TYPE>`

The default project type. this will be used if you didn't specify the project type when creating a version. the available project type will be `File`, `Gettext`, `Podir`, `Properties`, `Utf8Properties`, `Xliff`, and `Xml'. See the [Zanata document](https://github.com/zanata/zanata-platform/blob/master/docs/user-guide/projects/project-types.md) for more details.

#### How to show the project information

##### Command

```shell
$ zanata-js project info [options] `<project id>` ...
```

##### Options

###### -l, --show-locales

Show available languages for translating.

#### How to create a version in the project

##### Command

```shell
$ zanata-js project create-version [options] `<project version id>` ...
```

##### Options

###### -p, --project=`<PROJECT ID>`

Specify the project id to create a version for. this can be omitted if you have this in zanata.xml.

###### -t, --project-type`<TYPE>`

Specify the project type for a version. this can be omitted if you want to inherit the default project type which you specified when creating a project.

#### How to show the project version information

##### Command

```shell
$ zanata-js project version-info [options] `<project version id>` ...
```

##### Options

###### -p, --project=`<PROJECT ID>`

Specify the project id to show the version information for. this can be omitted if you have this in zanata.xml.

###### -l, --show-locales

Show available languages for translating on the version.

#### How to pull translations from the Zanata

##### Command

```shell
$ zanata-js project pull [options]
```

##### Options

###### -p, --project=`<PROJECT ID>`

Specify the project id to pull the translations for. this can be omitted if you have this in zanata.xml.

###### -V, --project-version=`<VERSION ID>`

Specify the project version id to pull the translations for. this can be omitted if you have this in zanata.xml.

###### -t, --project-type=`<TYPE>`

Specify the project type. this can be omitted if you have this in zanata.xml.

###### -T, --pull-type=`<TYPE>`

Specify what the sort of documentation you want to pull. the available type is, `source`, `trans`, and `both`. the default pull type is `trans`.

###### -l, --locales=<LOCALE,...>

Specify the locales you want to pull.

###### -k, --create-skeletons

Create skeleton entries for languages which haven't yet been translated on the Zanata. languages that have any translations will be pulled and stored into the file by default.

###### --pot-dir=`<DIR>`

Specify the place to store the POT file.

###### --po-dir=`<DIR>`

Specify the place to store the PO files.

###### -f, --force

Force to transfer the data.

###### -v, --verbose

Show more progress message verbosely.

###### --dry-run

Do not store the data into the files.

#### How to push translations to the Zanata

##### Command

```shell
$ zanata-js project push [options]
```

##### Options

###### -p, --project=`<PROJECT ID>`

Specify the project id to push documents to the Zanata. this can be omitted if you have this in zanata.xml.

###### -V, --project-version`<VERSION ID>`

Specify the project version id to push documents to the Zanata. this can be omitted if you have this in zanata.xml.

###### -t, --project-type=`<TYPE>`

Specify the project type. this can be omitted if you have this in zanata.xml.

###### -T, --push-type=`<TYPE>`

Specify what the sort of documentation you want to push. the available type is, `source`, `trans`, and `both`. the default push type is `trans`.

###### -l, --locales=`<LOCALE, ...>`

Specify the locales you want to push.

###### --pot-dir=`<DIR>`

Specify the place to push the POT file on.

###### --po-dir=`<DIR>`

Specify the place to push the PO files on.

###### --copy-trans=`<BOOLEAN>`

Copy the latest translations from equivalent messages/documents in the translation memory of the Zanata.

###### --dry-run

Do not push the documentation to the Zanata actually.

###### -v, --verbose

Show more progress messages verbosely.

#### How to show the statistics for the project

##### Command

```shell
$ zanata-js project stats [options]
```

##### Options

###### -p, --project=`<PROJECT ID>`

Specify the project id get statistics for from the Zanata. this can be omitted if you have this in zanata.xml.

###### -V, --project-version=`<VERSION ID>`

Specify the project version id get statistics for from the Zanata. this can be omitted if you have this in zanata.xml.

###### -D, --doc-id=`<DOC ID>`

Specify the document id get statistics for from the Zanata. this is optional and may only helps if you have multiple documents in the version.

###### -d, --detail

Show the detailed statistics.

###### -w, --word

Show the word-level statistics.

###### -l, --locales=`<LOCALE, ...>`

Specify the locales you want to get statistics for.

###### -v, --verbose

Show more progress messages verbosely.

## Getting Started with API

```js
const ZanataClient = require('zanata-js').ZanataClient;

let project = new ZanataClient.Project(options)
  .on('fail', (e) => console.error)
  .on('data', (d) => {
    ...
  })
  .on('end', (r) => {
    ...
  })
  .pull({project: 'project id', version: 'version id', ....});
```

For sample code, zanata-js CLI or [grunt-zanata-js](https://bitbucket.org/tagoh/grunt-zanata-js) may helps to speed up understanding.

## API Reference

See https://tagoh.bitbucket.io/zanata-js/ for more details.

# License

MIT
