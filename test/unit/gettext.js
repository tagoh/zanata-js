'use strict';

const assert = require('assert');
const Gettext = require('../../lib/zanata/gettext.js').Gettext;
const Resource = require('../../lib/zanata/dataType/resource.js');
const Translations = require('../../lib/zanata/dataType/translations.js');
const fs = require('fs');
const path = require('path');

describe('gettext', function() {
  describe('po2json', function() {
    it('Convert POT to JSON', function(done) {
      console.log(__dirname);
      let pot = fs.readFileSync(path.join(__dirname, 'test.pot'));
      let json = JSON.parse(fs.readFileSync(path.join(__dirname, 'test.pot.json')));
      Gettext.po2json('test', 'pot', pot)
        .then((data) => {
          assert.deepEqual(data, new Resource(json));
          done();
        })
        .catch(done);
    });
    it('Convert PO to JSON', function(done) {
      let po = fs.readFileSync(path.join(__dirname, 'test.po'));
      let json = JSON.parse(fs.readFileSync(path.join(__dirname, 'test.po.json')));
      Gettext.po2json('test', 'po', po)
        .then((data) => {
          assert.deepEqual(data, new Translations(json));
          done();
        })
        .catch(done);
    });
    it('Convert incomplete POT to JSON', function(done) {
      let pot = fs.readFileSync(path.join(__dirname, 'test_incomplete.pot'));
      let json = JSON.parse(fs.readFileSync(path.join(__dirname, 'test_incomplete.pot.json')));
      Gettext.po2json('test_incomplete', 'pot', pot)
        .then((data) => {
          assert.deepEqual(data, new Resource(json));
          done();
        })
        .catch(done);
    });
  });
  describe('json2po', function() {
    it('Convert JSON to POT', function(done) {
      let json = JSON.parse(fs.readFileSync(path.join(__dirname, 'test.pot.json')));
      let pot = fs.readFileSync(path.join(__dirname, 'test.pot'));
      Gettext.json2po(json)
        .then((data) => {
          Gettext.po2json('test', 'pot', data)
            .then((d) => {
              assert.deepEqual(d, new Resource(json));
              done();
            })
            .catch(done);
        })
        .catch(done);
    });
    it('Convert JSON to PO', function(done) {
      let json = JSON.parse(fs.readFileSync(path.join(__dirname, 'test.po.json')));
      let json2 = JSON.parse(fs.readFileSync(path.join(__dirname, 'test2.po.json'))); // simply containing textFlows with 'json'
      let po = fs.readFileSync(path.join(__dirname, 'test.po'));
      Gettext.json2po(json2)
        .then((data) => {
          Gettext.po2json('test', 'po', data)
            .then((d) => {
              assert.deepEqual(d, json);
              done();
            })
            .catch(done);
        })
        .catch(done);
    });
    /*
    it('test', function(done) {
      this.timeout(60000);
      const project = require('../../lib/zanata/project.js').Project;
      let p = new project({url: 'https://translate.zanata.org', username: 'tagoh'});
      p.on('fail', (e) => console.error(e.toString()))
        .on('warning', (e) => console.warn(e))
        .on('data_pull', (d) => {
          let pot = fs.readFileSync(path.join(__dirname, '../../po3/fh-fhc.pot'));
          Gettext.po2json('fh-fhc', 'pot', pot)
            .then((dd) => {
              assert.deepEqual(dd, d.object);
              done();
            })
            .catch(done);
        })
        .pull({project: 'fh-fhc', version: 'master', pullType: 'source', potdir: path.join(__dirname, '../../po3')});
    });
    */
  });
});
