'use strict';
const expect = require('chai').expect;
const assert = require('assert');
const r = require('../../../lib/zanata/api/projectsResource.js').ProjectsResource;

describe('projectsResource', function() {
  describe('get', function() {
    it('No URL is invalid', function() {
      expect(function() {
	new r();
      }).to.throw('No server URL is specified.');
    });
    it('Empty URL is invalid', function() {
      expect(function() {
	new r('');
      }).to.throw('No server URL is specified.');
    });
    it('Exception on invalid URL', function() {
      expect(function() {
	new r('foo');
      }).to.throw('Invalid URL: foo');
    });
    it('Get projects list', function() {
      this.timeout(60000);
      let i = new r('https://translate.zanata.org');
      return i.get()
        .then((data) => {
          expect(data).to.not.be.null;
          expect(data.data).to.not.be.null;
          expect(data.json).to.not.be.null;
	  expect(data.json).to.be.instanceof(Array);
        });
    });
  });
});
