'use strict';
const assert = require('assert');
const ar = require('../../../lib/zanata/api/accountResource.js').AccountResource;

describe('accountResource', function() {
  describe('get', function() {
    it('Get an user information', function(done) {
      let i = new ar('https://translate.stage.zanata.org');
      let fn = () => {
        return i.get('zanatajs');
      };
      assert.doesNotThrow(fn, Error, "Access from non-authorized user");
      done();
    });
  });
  describe('put', function() {
    it('Update the user information', function(done) {
      done();
    });
  });
});
