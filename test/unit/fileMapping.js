'use strict';

const assert = require('assert');
const expect = require('chai').expect;
const fmr = require('../../lib/zanata/fileMapping.js').FileMappingRule;
const config = require('../../lib/zanata/config.js').Config;
const path = require('path');

describe('fileMappingRule', function() {
  describe('setMapping', function() {
  });
  describe('getPath', function() {
    it('default mapping', function(done) {
      let f = new fmr();
      assert.deepEqual(f.getPath('gettext', {}), '{path}/{locale_with_underscore}.{extension}');
      assert.deepEqual(f.getPath('podir', {}), '{locale}/{path}/{filename}.{extension}');
      assert.deepEqual(f.getPath('properties', {}), '{path}/{filename}_{locale_with_underscore}.{extension}');
      assert.deepEqual(f.getPath('utf8properties', {}), '{path}/{filename}_{locale_with_underscore}.{extension}');
      assert.deepEqual(f.getPath('xliff', {}), '{path}/{filename}_{locale_with_underscore}.{extension}');
      assert.deepEqual(f.getPath('xml', {}), '{path}/{filename}_{locale_with_underscore}.{extension}');
      assert.deepEqual(f.getPath('file', {}), '{locale}/{path}/{filename}.{extension}');
      expect(f.getPath.bind(f, 'foobar', {})).to.throw('Unknown mapping type: foobar');
      assert.deepEqual(f.getPath('gettext', {path: './po', locale: 'ja', extension: 'po'}), './po/ja.po');
      done();
    });
    it('custom mapping', function(done) {
      let c = new config(path.join(__dirname, 'customMapping.xml'));
      let f = new fmr(c.get('rules'));
      assert.deepEqual(f.getPath('gettext', {source: 'po/foo.pot'}), '{path}/foo_{locale_with_underscore}.{extension}');
      done();
    });
    it('custom mappings', function(done) {
      let c = new config(path.join(__dirname, 'customMappings.xml'));
      let f = new fmr(c.get('rules'));
      assert.deepEqual(f.getPath('gettext', {source: 'po/foo.pot'}), '{path}/foopot_{locale_with_underscore}.{extension}');
      assert.deepEqual(f.getPath('gettext', {source: 'po/foo.po'}), '{path}/foopo_{locale_with_underscore}.{extension}');
      done();
    });
  });
  describe('getRealPath', function() {
  });
});
