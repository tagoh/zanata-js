'use strict';

const CommonAPIHandler = require('./common.js');

class StatisticsResource extends CommonAPIHandler {

  constructor(server) {
    super(server, '');
    this.baseUri = this.target + '/stats/proj/${project}/iter/${iteration}';
    this.uri = this.baseUri + '?detail=${detail}&word=${word}${locale}';
    this.docUri = this.baseUri + '/doc/${doc}?word=${word}${locale}';
    this.contribUri = '/stats/proj/${project}/version/${version}/contributor/${user}/${date}?includeAutomatedEntry=${automatedEntry}';
  }

  get(projectId, versionId, params) {
    let detail = params.detail || false;
    let word = params.word || false;
    let locales = params.locales || '';
    if (locales && locales instanceof Array)
      locales = locales.join('&locale=');
    if (locales)
      locales = '&locale=' + locales;
    return this._get(this.uri, {path: {project: projectId, iteration: versionId, detail: detail, word: word, locale: locales}},
                     (d) => {
                       try {
                         d.json = JSON.parse(d.data);
                       } catch (e) {
                       }
                       return d;
                     });
  }

  getDoc(projectId, versionId, docId, params) {
    let word = params.word || false;
    let locales = params.locales || '';
    if (locales && locales instanceof Array)
      locales = locales.join('&locale=');
    if (locales)
      locales = '&locale=' + locales;
    return this._get(this.docUri, {path: {project: projectId, iteration: versionId, doc: docId, word: word, locale: locales}},
                     (d) => {
                       try {
                         d.json = JSON.parse(d.data);
                       } catch (e) {
                       }
                       return d;
                     });
  }

  getByUser(projectId, versionId, user, data, params, callback) {
    let autoEntry = params.automatedEntry || false;
    return this._get(this.contribUri, {path: {project: projectId, version: versionId, user: user, date: date, automatedEntry: autoEntry}},
                     (d) => {
                       try {
                         d.json = JSON.parse(d.data);
                       } catch (e) {
                       }
                       return d;
                     });
  }

}

exports.StatisticsResource = StatisticsResource;
