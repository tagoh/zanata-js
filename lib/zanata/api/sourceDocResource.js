'use strict';

const CommonAPIHandler = require('./common.js');
const ResourceMeta = require('../dataType/resourceMeta.js');
const Resource = require('../dataType/resource.js');

class SourceDocResource extends CommonAPIHandler {

  constructor(server) {
    super(server, '');
    this.uri = this.target + '/projects/p/${project}/iterations/i/${iteration}/r';
    this.docUri = this.target + '/projects/p/${project}/iterations/i/${iteration}/r/${id}';
    this.metaUri = this.target + '/projects/p/${project}/iterations/i/${iteration}/r/${id}/meta';
    this.resUri = this.target + '/projects/p/${project}/iterations/i/${iteration}/resource';
    this.resMetaUri = this.target + '/projects/p/${project}/iterations/i/${iteration}/resource/meta';
  }

  head(projectId, versionId) {
    return this._head(this.uri, {path: {project: projectId, iteration: versionId}}, (d) => {
      try {
        d.json = JSON.parse(d.data);
      } catch(e) {
      }
      return d;
    });
  }

  get(projectId, versionId) {
    return this._get(this.uri,
                     {path: {project: projectId, iteration: versionId},
                      qs: {ext: ['gettext', 'comment']}},
                     (d) => {
                       try {
                         d.json = JSON.parse(d.data);
                         d.object = ResourceMeta.fromJSON(d.json);
                       } catch (e) {
                       }
                       return d;
                     });
  }

  post(projectId, versionId, data) {
    return this._post(this.uri,
                      {path: {project: projectId, iteration: versionId},
                       qs: {ext: ['gettext', 'comment']},
                       headers: {'Content-Type': 'application/json'},
                       payload: JSON.stringify(data)},
                      (d) => d);
  }

  getDoc(projectId, versionId, name) {
    this._deprecated();
    return this._get(this.docUri, {path: {project: projectId, iteration: versionId, id: name},
                                   qs: {ext: ['gettext', 'comment']}},
                     (d) => {
                       try {
                         d.json = JSON.parse(d.data);
                         d.object = Resource.fromJSON(d.json);
                       } catch (e) {
                       }
                       return d;
                     });
  }

  putDoc(projectId, versionId, name, data) {
    this._deprecated();
    return this._put(this.docUri, {path: {project: projectId, iteration: versionId, id: name},
                                   qs: {ext: ['gettext', 'comment']},
                                   headers: {'Content-Type': 'application/json'},
                                   payload: JSON.stringify(data)},
                     (d) => d);
  }

  deleteDoc(projectId, versionId, name) {
    this._deprecated();
    return this._delete(this.docUri, {path: {project: projectId, iteration: versionId, id: name},
                                      qs: {ext: ['gettext', 'comment']}},
                        (d) => d);
  }

  getMeta(projectId, versionId, docId) {
    this._deprecated();
    return this._get(this.metaUri, {path: {project, projectId, iteration: versionId, id: docId},
                                    qs: {ext: ['gettext', 'comment']}},
                     (d) => {
                       try {
                         d.json = JSON.parse(d.data);
                       } catch (e) {
                       }
                       return d;
                     });
  }

  putMeta(projectId, versionId, docId, meta) {
    this._deprecated();
    return this._put(this.metaUri, {path: {project: projectId, iteration: versionId, id: docId},
                                    qs: {ext: ['gettext', 'comment']},
                                    headers: {'Content-Type': 'application/json'},
                                    payload: JSON.stringify(meta)},
                     (d) => d);
  }

  deleteResource(projectId, versionId, docId) {
    return this._delete(this.resUri, {path: {project: projectId, iteration: versionId},
                                      qs: {docId: docId},
                                      headers: {'Content-Type': 'application/json'}},
                        (d) => d);
  }

  getResource(projectId, versionId, docId) {
    return this._get(this.resUri, {path: {project: projectId, iteration: versionId},
                                   qs: {docId: docId, ext: ['gettext', 'comment']},
                                   headers: {'Content-Type': 'application/json'}},
                     (d) => {
                       try {
                         d.json = JSON.parse(d.data);
                         d.object = Resource.fromJSON(d.json);
                       } catch (e) {
                       }
                       return d;
                     });
  }

  putResource(projectId, versionId, docId, data, copyTrans) {
    return this._put(this.resUri, {path: {project: projectId, iteration: versionId},
                                   qs: {copyTrans: copyTrans, docId: docId, ext: ['gettext', 'comment']},
                                   headers: {'Content-Type': 'application/json'},
                                   payload: JSON.stringify(data)},
                     (d) => d);
  }

  getResourceMeta(projectId, versionId, docId) {
    return this._get(this.resMetaUri, {path: {project: projectId, iteration: versionId},
                                       qs: {docId: docId, ext: ['gettext', 'comment']}},
                     (d) => {
                       try {
                         d.json = JSON.parse(d.data);
                         d.object = ResourceMeta.fromJSON(d.json);
                       } catch (e) {
                       }
                       return d;
                     });
  }

  putResourceMeta(projectId, versionId, docId, data) {
    return this._put(this.resMetaUri, {path: {project: projectId, iteration: versionId},
                                       qs: {docId: docId, ext: ['gettext', 'comment']},
                                       headers: {'Content-Type': 'application/json'},
                                       payload: JSON.stringify(data)},
                     (d) => d);
  }
}

exports.SourceDocResource = SourceDocResource;
