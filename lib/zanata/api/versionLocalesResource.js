'use strict';

const CommonAPIHandler = require('./common.js');
const LocaleDetails = require('../dataType/localeDetails.js');
const SourceLocaleDetails = require('../dataType/sourceLocaleDetails.js');

class VersionLocalesResource extends CommonAPIHandler {

  constructor(server) {
    super(server, 'project.locales');
    this.uri = this.target + '/projects/p/${project}/iterations/i/${iteration}/locales';
  }

  get(project, iterationId) {
    return this._get(this.uri, {path: {project: project, iteration: iterationId}}, (d) => {
      try {
        d.json = JSON.parse(d.data);
        d.object = d.json.map((x) => LocaleDetails.fromJSON(x));
      } catch(e) {
      }
      return d;
    });
  }

  getSource(project, iterationId) {
    return this._get(this.uri + '/source', {path: {project: project, iteration: iterationId},
                                            headers: {Accept: 'application/json'}},
                     (d) => {
                       try {
                         d.json = JSON.parse(d.json);
                         d.object = d.json.map((x) => SourceLocaleDetails.fromJSON(x));
                       } catch(e) {
                       }
                       return d;
                     });
  }

}

exports.VersionLocalesResource = VersionLocalesResource;
