'use strict';

const CommonAPIHandler = require('./common.js');
const ProjectVersion = require('../dataType/projectVersion.js');
const ResourceMeta = require('../dataType/resourceMeta.js');
const LocaleDetails = require('../dataType/localeDetails.js');
const User = require('../dataType/user.js');
const TransUnitStatus = require('../dataType/transUnitStatus.js');

class ProjectVersionResource extends CommonAPIHandler {

  constructor(server) {
    super(server, 'project.iteration');
    this.uri = this.target + '/project/${project}/version/${version}';
  }

  get(project, versionId) {
    return this._get(this.uri, {path: {project: project, version: versionId}}, (d) => {
      try {
        d.json = JSON.parse(d.data);
        d.object = projectVersion.fromJSON(d.json);
      } catch (e) {
      }
      return d;
    });
  }

  head(project, versionId) {
    return this._head(this.uri, {path: {project: project, version: versionId}}, (d) => {
      try {
        d.json = JSON.parse(d.data);
      } catch (e) {
      }
      return d;
    });
  }

  put(project, versionId, data) {
    return this._put(this.uri, {path: {project: project, version: versionId},
                                headers: {'Content-Type': 'application/vnd.zanata.project.iteration+json',
                                          Accept: 'application/json'},
                                payload: JSON.stringify(data)},
                     (d) => d);
  }

  getConfig(project, versionId) {
    return this._get(this.uri + '/config', {path: {project: project, version: versionId},
                                            headers: {Accept: 'application/xml'}},
                     (d) => d);
  }

  getDocList(project, versionId) {
    return this._get(this.uri + '/docs', {path: {project: project, version: versionId},
                                          headers: {Accept: 'application/json'}},
                     (d) => {
                       try {
                         d.json = JSON.parse(d.data);
                         d.object = d.json.map((x) => ResourceMeta.fromJSON(x));
                       } catch (e) {
                       }
                       return d;
                     });
  }

  getLocalesList(project, versionId) {
    return this._get(this.uri + '/locales', {path: {project: project, version: versionId},
                                             headers: {Accept: 'application/vnd.zanata.version.locales+json'}},
                     (d) => {
                       try {
                         d.json = JSON.parse(d.data);
                         d.object = d.json.map((x) => LocaleDetails.fromJSON(x));
                       } catch (e) {
                       }
                       return d;
                     });
  }

  getContributorList(project, versionId, From, To) {
    return this._get(this.uri + '/contributors/${from}..${to}', {path: {project: project,
                                                                        version: versionId,
                                                                        from: From,
                                                                        to: To},
                                                                 headers: {Accept: 'application/vnd.zanata.version+json'}},
                     (d) => {
                       try {
                         d.json = JSON.parse(d.data);
                         d.object = d.json.map((x) => User.fromJSON(x));
                       } catch (e) {
                       }
                       return d;
                     });
  }

  postStatus(project, versionId, docId, locale, data) {
    return this._post(this.uri + '/doc/${docId}/status/${locale}', {path: {project: project,
                                                                           version: versionId,
                                                                           docId: docId,
                                                                           locale: locale},
                                                                    headers: {Accept: 'application/vnd.zanata.tu.resource+json'},
                                                                    payload: JSON.stringify(data)},
                      (d) => {
                        try {
                          d.json = JSON.parse(d.data);
                          d.object = d.json.map((x) => TransUnitStatus.fromJSON(x));
                        } catch (e) {
                        }
                        return d;
                      });
  }

}

exports.ProjectVersionResource = ProjectVersionResource;
