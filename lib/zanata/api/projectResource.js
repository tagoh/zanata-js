'use strict';

const CommonAPIHandler = require('./common.js');
const Project = require('../dataType/project.js');
const QualifiedName = require('../dataType/qualifiedName.js');

class ProjectResource extends CommonAPIHandler {

  constructor(server) {
    super(server, 'project');
    this.uri = this.target + '/projects/p/${project}';
    this.statusMessages[404] = 'Project not found';
  }

  head(project) {
    return this._head(this.uri, {path: {project: project}}, (d) => {
      try {
        d.json = JSON.parse(d.data);
      } catch(e) {
      }
      return d;
    });
  }

  get(project) {
    return this._get(this.uri, {path: {project: project}}, (d) => {
      try {
        d.json = JSON.parse(d.data);
        d.object = Project.fromJSON(d.json);
      } catch(e) {
      }
      return d;
    });
  }

  put(data) {
    return this._put(this.uri, {path: {project: data.id},
                                headers: {Accept: 'application/json',
                                          'Content-Type': 'application/vnd.zanata.project+json'},
                                payload: JSON.stringify(data)},
                     (d) => d);
  }

  getQualifiedName(project) {
    return this._get(this.uri + '/glossary/qualifiedName', {path: {project: project}}, (d) => {
      try {
        d.json = JSON.parse(d.data);
        d.object = QualifiedName.fromJSON(d.json);
      } catch (e) {
      }
      return d;
    });
  }

}

exports.ProjectResource = ProjectResource;
