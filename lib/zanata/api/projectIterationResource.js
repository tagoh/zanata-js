'use strict';

const CommonAPIHandler = require('./common.js');

class ProjectIterationResource extends CommonAPIHandler {

  constructor(server) {
    super(server, 'project.iteration');
    this.uri = this.target + '/projects/p/${project}/iterations/i/${iteration}';
  }

  head(project, iterationId) {
    this._deprecated();
    return this._head(this.uri, {path: {project: project, iteration: iterationId}}, (d) => {
      try {
        d.json = JSON.parse(d.data);
      } catch(e) {
      }
      return d;
    });
  }

  get(project, iterationId) {
    this._deprecated();
    return this._get(this.uri, {path: {project: project, iteration: iterationId}}, (d) => {
      try {
        d.json = JSON.parse(d.data);
      } catch(e) {
      }
      return d;
    });
  }

  put(project, iterationId, data) {
    this._deprecated();
    return this._put(this.uri, {path: {project: project, iteration: iterationId},
                                headers: {'Content-Type': 'application/vnd.zanata.project.iteration+json',
                                          Accept: 'application/json'},
                                payload: JSON.stringify(data)},
                     (d) => d);
  }

}

exports.ProjectIterationResource = ProjectIterationResource;
