'use strict';

const CommonAPIHandler = require('./common.js');

class GlossaryResource extends CommonAPIHandler {

  constroctor(server, params) {
    super(server, 'glossary', params);
    this.uri = this.target + '/glossary';
  }

  getQualifiedName() {
    return this._get(this.uri + '/qualifiedName', {}, (d) => {
      try {
        d.json = JSON.parse(d.data);
      } catch (e) {
      }
      return d;
    });
  }

  getInfo(qualifiedName) {
    return this._get(this.uri + '/info', {qs: {qualifiedName: qualifiedName}}, (d) => {
      try {
        d.json = JSON.parse(d.data);
      } catch (e) {
      }
      return d;
    });
  }

  getEntries(params) {
    query = {};
    if (params) {
      if (params.filter) {
        query['filter'] = params.filter;
      }
      if (params.sort) {
        query['sort'] = params.sort;
      }
      if (params.transLocale) {
        query['transLocale'] = params.transLocale;
      }
    }
    query['page'] = (params && params.page ? params.page : '1');
    query['qualifiedName'] = (params && params.qualifiedName ? params.qualifiedName : 'global/default');
    query['sizePerPage'] = (params && params.sizePerPage ? params.sizePerPage : '1000');
    query['srcLocale'] = (params && params.srcLocale ? params.srcLocale : 'en-US');
    return this._get(this.uri + '/entries', {qs: query}, (d) => {
      try {
        d.json = JSON.parse(d.data);
      } catch (e) {
      }
      return d;
    });
  }

  getFile(fileType, locales, qualifiedName) {
    query = {};
    if (fileType == undefined) {
      fileType = 'csv';
    }
    if (qualifiedName == undefined) {
      qualifiedName = 'global/default';
    }
    if (locales) {
      query['locales'] = locales.join(',');
    }
    query['fileType'] = fileType;
    query['qualifiedName'] = qualifiedName;
    return this._get(this.uri + '/file', {qs: query,
                                headers: {Accept: 'application/octet-stream'}},
                     ((d) => d));
  }

  postEntries(locale, qualifiedName, entry) {
    query = {};
    if (qualifiedName == undefined) {
      qualifiedName = 'global/default';
    }
    query['locale'] = locale;
    query['qualifiedName'] = qualifiedName;
    return this._post(this.uri + '/entries', {qs: query,
                                 headers: {'Content-Type': 'application/json',
                                           Accept: 'application/json'},
                                 payload: JSON.stringify(entry)},
                      (d) => {
                        try {
                          d.json = JSON.parse(d.data);
                        } catch (e) {
                        }
                        return d;
                      });
  }

  postGlossary() {
  }

  deleteEntries(id) {
    return this._delete(this.uri + '/entries/${id}', {path: {id: id},
                                                      headers: {Accept: 'application/json'}},
                        (d) => {
                          try {
                            d.json = JSON.parse(d.data);
                          } catch (e) {
                          }
                          return d;
                        });
  }

  deleteGlossary(qualifiedName) {
    if (qualifiedName == undefined) {
      qualifiedName = 'global/default';
    }
    return this._delete(this.uri, {qs: {qualifiedName: qualifiedName}},
                        (d) => {
                          try {
                            d.json = JSON.parse(d.data);
                          } catch (e) {
                          }
                          return d;
                        });
  }

}

exports.GlossaryResource = GlosssaryResource;
