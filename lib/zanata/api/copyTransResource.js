'use strict';

const CommonAPIHandler = require('./common.js');

class CopyTransResource extends CommonAPIHandler {

  constructor(server, params) {
    super(server, '', params);
    this.uri = this.target + '/copytrans/proj/${project}/iter/${iteration}/doc/${docid}';
  }

  post(projectId, versionId, docId) {
    return this._post(this.uri, {path: {project: projectId,
                                        iteration: versionId,
                                        docid: docId},
                                 headers: {Accept: 'application/json'}},
                      (d) => {
                        try {
                          d.json = JSON.parse(d.data);
                        } catch (e) {
                        }
                        return d;
                      });
  }

  get(projectId, versionId, docId) {
    return this._get(this.uri, {path: {project: projectId,
                                       iteration: versionId,
                                       docid: docId},
                                headers: {Accept: 'application/json'}},
                     (d) => {
                       try {
                         d.json = JSON.parse(d.data);
                       } catch (e) {
                       }
                       return d;
                     });
  }

}
