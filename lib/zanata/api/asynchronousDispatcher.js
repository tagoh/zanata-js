'use strict';

const CommonAPIHandler = require('./common.js');

class AsynchronousDispatcher extends CommonAPIHandler {

  constructor(server, params) {
    super(server, '', params);
    this.uri = this.target + '/${jobid}';
  }

  del(id) {
    return this._delete(this.uri, {path: {jobid: id}}, ((d) => d));
  }

  post(id, wait) {
    if (wait == undefined) {
      wait = -1;
    }
    return this._post(this.uri, {path: {jobid: id},
                                 qs: {wait: wait},
                      (d) => {
                        try {
                          d.json = JSON.parse(d.data);
                        } catch (e) {
                        }
                        return d;
                      });
  }

  get(id, wait) {
    if (wait == undefined) {
      wait = -1;
    }
    return this._get(this.uri, {path: {jobid: id},
                                qs: {wait: wait},
                     (d) => {
                       try {
                         d.json = JSON.parse(d.data);
                       } catch (e) {
                       }
                       return d;
                     });
  }

}

exports.AsynchronousDispatcher = ASynchronousDispatcher;
