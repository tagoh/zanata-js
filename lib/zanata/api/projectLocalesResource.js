'use strict';

const CommonAPIHandler = require('./common.js');
const LocaleDetails = require('../dataType/localeDetails.js');
const SourceLocaleDetails = require('../dataType/sourceLocaleDetails.js');

class ProjectLocalesResource extends CommonAPIHandler {

  constructor(server) {
    super(server, 'project.locales');
    this.uri = this.target + '/projects/p/${project}/locales';
    this.sourceUrl = this.target + '/projects/p/${project}/locales/source';
  }

  get(project) {
    return this._get(this.uri, {path: {project: project}}, (d) => {
      try {
        d.json = JSON.parse(d.data);
        d.object = d.json.map((x) => LocaleDetails.fromJSON(x));
      } catch(e) {
      }
      return d;
    });
  }

  getSource(project) {
    return this._get(this.sourceUrl, {path: {project: project},
                                      headers: {'Accept': 'application/json'}},
                     (d) => {
                       try {
                         d.json = JSON.parse(d.data);
                         d.object = SourceLocaleDetails.fromJSON(d.json);
                       } catch(e) {
                       }
                       return d;
                     });
  }

}

exports.ProjectLocalesResource = ProjectLocalesResource;
