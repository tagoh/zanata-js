'use strict';

const CommonAPIHandler = require('./common.js');

class ProjectConfigIterationResource extends CommonAPIHandler {

  constructor(server) {
    super(server, 'project.iteration');
    this.uri = this.target + '/projects/p/${project}/iterations/i/${iteration}/config';
  }

  head(project, iterationId) {
    return this._head(this.uri, {path: {project: project, iteration: iterationId}}, (d) => {
      try {
        d.json = JSON.parse(d.data);
      } catch(e) {
      }
      return d;
    });
  }

  get(project, iterationId) {
    return this._get(this.uri, {path: {project: project, iteration: iterationId}}, (d) => {
      try {
        d.json = JSON.parse(d.data);
      } catch(e) {
      }
      return d;
    });
  }

  put(project, iterationId) {
    return this._put(this.uri, {path: {project: project, iteration: iterationId}}, (d) => {
      try {
        d.json = JSON.parse(d.data);
      } catch(e) {
      }
      return d;
    });
  }

}

exports.ProjectConfigIterationResource = ProjectConfigIterationResource;
