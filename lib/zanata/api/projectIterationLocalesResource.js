'use strict';

const CommonAPIHandler = require('./common.js');

class ProjectIterationLocalesResource extends CommonAPIHandler {

  constructor(server) {
    super(server, 'project.locales');
    this.uri = this.target + '/projects/p/${project}/iterations/i/${iteration}/locales';
  }

  get(project, iterationId) {
    this.deprecated();
    return this._get(this.uri, {path: {project: project, iteration: iterationId}}, (d) => {
      try {
        d.json = JSON.parse(d.data);
      } catch(e) {
      }
      return d;
    });
  }

}

exports.ProjectIterationLocalesResource = ProjectIterationLocalesResource;
