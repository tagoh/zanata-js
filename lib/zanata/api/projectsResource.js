'use strict';

const CommonAPIHandler = require('./common.js');
const Project = require('../dataType/project.js');

class ProjectsResource extends CommonAPIHandler {

  constructor(server) {
    super(server, 'projects');
    this.uri = this.target + '/projects';
  }

  get() {
    return this._get(this.uri, {}, (d) => {
      try {
        d.json = JSON.parse(d.data);
        d.object = d.json.map((x) => Project.fromJSON(x));
      } catch(e) {
      }
      return d;
    });
  }

}

exports.ProjectsResource = ProjectsResource;
