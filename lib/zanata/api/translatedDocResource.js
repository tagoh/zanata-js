'use strict';

const util = require('util');
const CommonAPIHandler = require('./common.js');

class TranslatedDocResource extends CommonAPIHandler {

  constructor(server) {
    super(server, '');
    this.uri = this.target + '/projects/p/${project}/iterations/i/${iteration}/r/${id}/translations/${locale}${ext}';
  }

  get(projectId, versionId, name, locale, skeletons) {
    this._deprecated();
    let skeleton = skeletons ? '&skeletons=true' : '';
    return this._get(this.uri, {path: {project: projectId,
                                       iteration: versionId,
                                       id: name,
                                       locale: locale,
                                       ext: '?ext=gettext&ext=comment' + skeleton},
                                headers: {Accept: 'application/json'}},
                     (d) => {
                       try {
                         d.json = JSON.parse(d.data);
                       } catch (e) {
                       }
                       return d;
                     });
  }

  delete(projectId, versionId, name, locale) {
    this._deprecated();
    return this._delete(this.uri, {path: {project: projectId,
                                          iteration: versionId,
                                          id: name,
                                          locale: locale,
                                          ext: '?ext=gettext&ext=comment'},
                                   headers: {Accept: 'application/json'}},
                        (d) => d);
  }

  put(projectId, versionId, name, locale, data, copyTrans) {
    this._deprecated();
    let ext = util.format('?ext=gettext&ext=comment&copyTrans=%s', copyTrans || false);
    return this._put(this.uri, {path: {project: projectId,
                                       iteration: versionId,
                                       id: name,
                                       locale: locale,
                                       ext: ext},
                                headers: {'Content-Type': 'application/json',
                                          Accept: 'application/json'},
                                payload: JSON.stringify(data)},
                     (d) => d);
  }

}

exports.TranslatedDocResource = TranslatedDocResource;
