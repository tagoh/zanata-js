'use strict';

const CommonAPIHandler = require('./common.js');

class AccountResource extends CommonAPIHandler {

  constructor(server, params) {
    super(server, 'account', params);
    this.uri = this.target + '/accounts/u/${user}';
  }

  get(user) {
    if (!this.isAuthInfoAvail()) {
      return Promise.reject(new Error('Authorization is required to access through this API'));
    }
    return this._get(this.uri, {path: {user: user}}, (d) => {
      try {
        d.json = JSON.parse(d.data);
      } catch(e) {
      }
      return d;
    });
  }

  put(user, callback) {
    return this._put(this.uri, {path: {user: user}}, (d) => {
      try {
        d.json = JSON.parse(d.data);
      } catch(e) {
      }
      return d;
    });
  }

}

exports.AccountResource = AccountResource;
