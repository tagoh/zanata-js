'use strict';

const CommonAPIHandler = require('./common.js');
const Translations = require('../dataType/translations.js');

class TranslatedDocumentsResource extends CommonAPIHandler {

  constructor(server) {
    super(server, '');
    this.uri = this.target + '/projects/p/${project}/iterations/i/${iteration}/resource/translations/${locale}';
  }

  delete(project, version, docId, locale) {
    return this._delete(this.uri, {path: {project: project,
                                          iteration: version,
                                          locale: locale},
                                   qs: {docId: docId}},
                        (d) => d);
  }

  get(project, version, docId, locale, skeletons) {
    let qs = {docId: docId,
              skeletons: skeletons ? true : false,
              ext: ['gettext', 'comment']};
    return this._get(this.uri, {path: {project: project,
                                       iteration: version,
                                       locale: locale},
                                qs: qs,
                                headers: {Accept: 'application/json'}},
                     (d) => {
                       try {
                         d.json = JSON.parse(d.data);
                         d.object = Translations.fromJSON(d.json);
                       } catch (e) {
                       }
                       return d;
                     });
  }

  put(project, version, docId, locale, data, merge) {
    if (merge !== 'auto' && merge !== 'import')
      throw new TypeError('Invalid merge type: merge type must be auto or import');
    let qs = {docId: docId,
              ext: ['gettext', 'comment'],
              merge: merge};
    return this._put(this.uri, {path: {project: project,
                                       iteration: version,
                                       locale: locale},
                                qs: qs,
                                headers: {'Content-Type': 'application/json',
                                          Accept: 'application/json'},
                                payload: JSON.stringify(data)},
                     (d) => d);
  }

}

exports.TranslatedDocumentsResource = TranslatedDocumentsResource;
