'use strict';

const request = require('request');
const util = require('util');
const url = require('url');
const interpolate = require('../../util/interpolate.js');
const debuglog = util.debuglog('zanata');
const debugdetail = util.debuglog('zanata-detail');
const stacktrace = require('stack-trace');

class CommonAPIHandler {

  constructor(server, mediaType, params) {
    this.statusMessages = {
      '304': 'Not modified',
      '400': 'Bad Request',
      '401': 'Unauthorized access',
      '404': 'Not found',
      '429': 'Too Many Request',
      '500': 'Unexpected error on the server'
    };
    let t = mediaType && mediaType.length > 0 ? 'application/vnd.zanata.' + mediaType + '+json' : 'application/json';
    if (server == undefined || server === '')
      throw new Error("No server URL is specified.");
    if (!server.match(/^https?:\/\//))
      throw new Error("Invalid URL: " + server);
    this.server = server;
    this.target = server.replace(/\/$/,'') + '/rest';
    this.options = {headers: {'Accept': t}};
  }

  _sendRequest(method, uri, _options, callback) {
    let options = _options || {path: {}};
    let path = interpolate(uri, options.path);
    let opts = {}
    let self = this;
    opts.uri = url.parse(path);
    opts['method'] = method;
    opts['headers'] = {};
    Object.assign(opts['headers'], this.options.headers);
    Object.assign(opts['headers'], options.headers);
    opts['qs'] = {};
    if (this.options.qs) {
      Object.assign(opts['qs'], this.options.qs);
    }
    if (options.qs) {
      Object.assign(opts['qs'], options.qs);
    }
    if (options.payload) {
      opts['body'] = options.payload;
    }
    opts['useQuerystring'] = true;

    debuglog('------');
    debuglog('Connecting to: ' + opts.uri.hostname + (opts.uri.port ? ':' + opts.uri.port : ''));
    debuglog('Query String: ' + (opts.qs ? util.inspect(opts.qs, {depth: null, showHidden: true}) : 'N/A'));
    debuglog('HTTP Method: ' + method);
    debuglog('Path: ' + opts.uri.pathname);
    debuglog('User: ' + (opts.headers['X-Auth-User'] ? opts.headers['X-Auth-User'] : 'N/A'));
    debuglog('ETag: ' + (opts.headers['If-None-Match'] ? opts.headers['If-None-Match'] : 'N/A'));
    debuglog('Payload: ' + (options.payload ? util.inspect(options.payload, {depth: null, showHidden: true}) : 'N/A'));

    let data = [];
    request(opts, (err, res, body) => {
      debugdetail('Headers: ' + (res.headers ? util.inspect(res.headers, {depth: null, showHidden: true}) : 'N/A'));
      debugdetail('Status: ' + res.statusCode);
      debugdetail('Body: ' + (body ? util.inspect(body, {depth: null, showHidden: true}) : 'N/A'));
      if (err) {
        callback(err);
      } else {
        if (res.headers)
          self.etag = res.headers['etag'];
        // it seems that no events emitted when status code was 429.
        if (res.statusCode === 429) {
          callback(new Error(this.httpStatusMessage(res)));
        } else {
          callback(null, {response: res, data: body, self: this});
        }
      }
    });
  }

  _deprecated() {
    let e = new Error();
    let stack = stacktrace.parse(e);

    console.warn("W: %s is deprecated.", stack[1].getFunctionName());
  }

  _head(uri, options, callback) {
    return new Promise((resolve,  reject) => {
      this._sendRequest('HEAD', uri, options, (e, d) => {
        if (e) {
          reject(e);
        } else {
          resolve(callback(d));
        }
      });
    });
  }

  _get(uri, options, callback) {
    return new Promise((resolve, reject) => {
      this._sendRequest('GET', uri, options, (e, d) => {
        if (e) {
          reject(e);
        } else {
          resolve(callback(d));
        }
      });
    });
  }

  _put(uri, options, callback) {
    return new Promise((resolve, reject) => {
      this._sendRequest('PUT', uri, options, (e, d) => {
        if (e) {
          reject(e);
        } else {
          resolve(callback(d));
        }
      });
    });
  }

  _post(uri, options, callback) {
    return new Promise((resolve, reject) => {
      this._sendRequest('POST', uri, options, (e, d) => {
        if (e) {
          reject(e);
        } else {
          resolve(callback(d));
        }
      });
    });
  }

  _delete(uri, options, callback) {
    return new Promise((resolve, reject) => {
      this._sendRequest('DELETE', uri, options, (e, d) => {
        if (e) {
          reject(e);
        } else {
          resolve(callback(d));
        }
      });
    });
  }

  setAuthUser(v) {
    if (v == undefined)
      delete this.options.headers['X-Auth-User'];
    else
      this.options.headers['X-Auth-User'] = v;
    return this;
  }

  setAuthToken(v) {
    if (v == undefined)
      delete this.options.headers['X-Auth-Token'];
    else
      this.options.headers['X-Auth-Token'] = v;
    return this;
  }

  isAuthInfoAvail() {
    return this.options.headers['X-Auth-User'] && this.options.headers['X-Auth-Token'];
  }

  getRealUri(uri, args) {
    let ret = uri;

    Object.keys(args).forEach(function(k) {
      let regexstr = util.format('\\${%s}', k);
      let re = new RegExp(regexstr, 'g');
      ret = ret.replace(re, args[k]);
    });
    return ret;
  }

  setETag(etag) {
    if (etag == undefined)
      delete this.options.headers['If-None-Match'];
    else
      this.options.headers['If-None-Match'] = etag;
    return this;
  }

  getETag() {
    return this.etag;
  }

  httpStatusMessage(response, data) {
    let ret;
    let self = this;

    Object.keys(this.statusMessages).forEach(function(k) {
      if (response && response.statusCode == k) {
        ret = self.statusMessages[k];
      }
    });
    if (ret == undefined) {
      if (response && response.statusCode !== 200)
        ret = 'Status code ' + response.statusCode + ': ' + data;
    }
    return ret;
  }

}

module.exports = CommonAPIHandler;
