'use strict';

const CommonAPIHandler = require('./common.js');
const VersionInfo = require('../dataType/versionInfo.js');

class VersionResource extends CommonAPIHandler {

  constructor(server) {
    super(server, 'version');
    this.uri = this.target + '/version';
  }

  get() {
    return this._get(this.uri, {}, (d) => {
      try {
        d.json = JSON.parse(d.data);
        d.object = VersionInfo.fromJSON(d.json);
      } catch(e) {
      }
      return d;
    });
  }

}

exports.VersionResource = VersionResource;
