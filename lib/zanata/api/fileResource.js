'use strict';

const CommonAPIHandler = require('./common.js');

class FileResource extends CommonAPIHandler {

  constructor(server) {
    super(server, '');
    this.uri = this.target + '/file';
    this.uriMap = {
      getAcceptedDocumentTypes: this.uri + '/accepted_document_types',
      download: this.uri + '/download/${download}',
      uploadSource: this.uri + '/source/${project}/${iteration}',
      downloadSource: this.uri + '/source/${project}/${iteration}/${fileType}',
      uploadTranslation: this.uri + '/translation/${project}/${iteration}/${locale}',
      downloadTranslation: this.uri + '/translation/${project}/${iteration}/${locale}/${fileType}'
    };
  }

  getAcceptedDocumentTypes() {
    return this._get(this.uriMap.getAcceptedDocumentTypes, {}, (d) => d);
  }

  download(id) {
    return this._get(this.uriMap.download, {path: {download: id}}, (d) => d);
  }

  uploadSource(projectId, versionId, data) {
    return this._post(this.uriMap.uploadSource,
                      {path: {project: projectId, iteration: versionId}},
                      (d) => d);
  }

  downloadSource(projectId, versionId, fileType) {
    return this._get(this.uriMap.downloadSource,
                     {path: {project: projectId, iteration: versionId, fileType: fileType},
                      headers: {Accept: 'application/octet-stream'}},
                     (d) => d);
  }

  uploadTranslation(projectId, versionId, locale, data) {
    return this._post(this.uriMap.uploadTranslation,
                      {path: {project: projectId, iteration: versionId, locale: locale}},
                      (d) => d);
  }

  downloadTranslation(projectId, versionId, locale, fileType) {
    return this._get(this.uriMap.downloadTranslation,
                     {path: {project: projectId, iteration: versionId, locale: locale, fileType: fileType}},
                     (d) => d);
  }

}

exports.FileResource = FileResource;
