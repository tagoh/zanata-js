'use strict';

const ContentState = {
  New: 'New',
  NeedReview: 'NeedReview',
  Translated: 'Translated',
  Approved: 'Approved',
  Rejected: 'Rejected'
};

module.exports = ContentState;
