'use strict';

const TextContainer = require('./textContainer.js');

class TextFlow extends TextContainer {

  constructor(obj) {
    super(obj);
    if (!('extensions' in obj) || !Array.isArray(obj.extensions))
      throw new TypeError('extensions is not an Array');
    if (!('id' in obj) || !('lang' in obj) || !('plural' in obj))
      throw new Error('missing required properties');
    this.id = obj.id;
    this.lang = obj.lang;
    this.plural = obj.plural;
    this.extensions = obj.extensions;
    this.revision = obj.revision;
  }

  static fromJSON(obj) {
    return new this(obj);
  }

}

module.exports = TextFlow;
