'use strict';

const LocaleDetails = require('./localeDetails.js');

class SourceLocaleDetails {

  constructor(obj) {
    if (!('docCount' in obj) || !('localeDetails' in obj))
      throw new TypeError('missing required properties');
    this.docCount = obj.docCount;
    this.localeDetails = new LocaleDetails(obj.localeDetails);
  }

  toJSON() {
    return Object.assign({}, this, {
      localeDetails: this.localeDetails.toString()
    });
  }

  static fromJSON(obj) {
    const sourceLocaleDetails = Object.create(SourceLocaleDetails.prototype);

    return Object.assign(sourceLocaleDetails, obj, {
      localeDetails: new LocaleDetails(obj.localeDetails)
    });
  }

}

module.exports = SourceLocaleDetails;
