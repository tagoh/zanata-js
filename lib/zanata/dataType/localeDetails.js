'use strict';

class LocaleDetails {

  constructor(obj) {
    if (!('localeId' in obj) || !('enabled' in obj) || !('enabledByDefault' in obj))
      throw new TypeError('missing required properties');
    this.localeId = obj.localeId;
    this.displayName = obj.displayName;
    this.alias = obj.alias;
    this.nativeName = obj.nativeName;
    this.enabled = obj.enabled;
    this.enabledByDefault = obj.enabledByDefault;
    this.pluralForms = obj.pluralForms;
    this.rtl = obj.rtl;
  }

  static fromJSON(obj) {
    return new LocaleDetails(obj);
  }

}

module.exports = LocaleDetails;
