'use strict';

const ProjectVersion = require('./projectVersion.js');
const Status = require('./status.js');

class Project {

  constructor(obj) {
    if (!('id' in obj) || !('defaultType' in obj) || !('name' in obj))
      throw new TypeError('no required properties');
    if (('links' in obj) && !Array.isArray(obj.links))
      throw new TypeError('links is not an Array');
    if (('iterations' in obj) && !Array.isArray(obj.iterations))
      throw new TypeError('iterations is not an Array');
    if (('status' in obj) && Object.keys(Status).map((x) => Status[x]).indexOf(obj.status) < 0)
      throw new TypeError('Invalid StatusType');
    this.id = obj.id;
    this.defaultType = obj.defaultType;
    this.name = obj.name;
    this.description = obj.description;
    this.sourceViewURL = obj.sourceViewURL;
    this.sourceCheckoutURL = obj.sourceCheckoutURL;
    this.links = obj.links || [];
    this.iterations = obj.iterations || [];
    this.status = obj.status;
  }

  toJSON() {
    return Object.assign({}, this, {
      iterations: this.iterations.map((x) => x.toString())
    });
  }

  static fromJSON(obj) {
    const project = Object.create(Project.prototype);

    return Object.assign(project, obj, {
      iterations: obj.iterations.map((x) => new ProjectVersion(x))
    });
  }
}

module.exports = Project;
