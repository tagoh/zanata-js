'use strict';

class QualifiedName {

  constructor(obj) {
    if (!('name' in obj))
      throw new TypeError('name is not in object');
    this.name = obj.name;
  }

  static fromJSON(obj) {
    return new QualifiedName(obj);
  }

}

module.exports = QualifiedName;
