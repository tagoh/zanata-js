'use strict';

const ContentState = require('./contentState.js');

class TransUnitStatus {

  constructor(obj) {
    if (!('id' in obj) || !('resId' in obj) || !('status' in obj))
      throw new TypeError('missing required properties');
    if (Object.keys(ContentState).map((x) => ContentState[x]).indexOf(obj.status) < 0)
      throw new TypeError('Invalid ContentState type');
    this.id = obj.id;
    this.resId = obj.resId;
    this.status = obj.status;
  }

  static fromJSON(obj) {
    return new this(obj);
  }

}

module.exports = TransUnitStatus;
