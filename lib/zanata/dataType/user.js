'use strict';

class User {

  constructor(obj) {
    if (!('username' in obj) || !('email' in obj) || !('name' in obj) || !('imageUrl' in obj))
      throw new TypeError('missing required properties');
    if (('languageTeams' in obj) && !Array.isArray(obj.languageTeams))
      throw new TypeError('languageTeams is not an Array');
    if (('roles' in obj) && !Array.isArray(obj.roles))
      throw new TypeError('roles is not an Array');
    this.username = obj.username;
    this.email = obj.email;
    this.name = obj.name;
    this.imageUrl = obj.imageUrl;
    this.languageTeams = obj.languageTeams;
    this.roles = obj.roles;
  }

  static fromJSON(obj) {
    return new this(obj);
  }

}

module.exports = User;
