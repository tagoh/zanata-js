'use strict';

const AbstractResource = require('./abstractResource.js');

class ResourceMeta extends AbstractResource {

  constructor(obj) {
    super(obj);
  }

  static fromJSON(obj) {
    return new this(obj);
  }

}

module.exports = ResourceMeta;
