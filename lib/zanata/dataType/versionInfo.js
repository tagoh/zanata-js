'use strict';

class VersionInfo {

  constructor(obj) {
    this.versionNo = obj.versionNo;
    this.buildTimeStamp = obj.buildTimeStamp;
    this.scmDescribe = obj.scmDescribe;
  }

  static fromJSON(obj) {
    return new this(obj);
  }

}

module.exports = VersionInfo;
