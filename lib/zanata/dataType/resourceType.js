'use strict';

const ResourceType = {
  FILE: 'FILE',
  DOCUMENT: 'DOCUMENT',
  PAGE: 'PAGE'
};

module.exports = ResourceType;
