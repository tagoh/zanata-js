'use strict';

const status = {
  ACTIVE: 'ACTIVE',
  READONLY: 'READONLY',
  OBSOLETE: 'OBSOLETE'
};

module.exports = status;
