'use strict';

const TextFlowTarget = require('./textFlowTarget.js');

class Translations {

  constructor(obj) {
    if (('links' in obj) && !Array.isArray(obj.links))
      throw new TypeError('links is not an Array');
    if (('extensions' in obj) && !Array.isArray(obj.extensions))
      throw new TypeError('extensions is not an Array');
    if (('textFlowTargets' in obj) && !Array.isArray(obj.textFlowTargets))
      throw new TypeError('textFlowTargets is not an Array');
    this.links = obj.links;
    this.extensions = obj.extensions;
    this.textFlowTargets = obj.textFlowTargets;
    this.revision = obj.revision;
  }

  static fromJSON(obj) {
    const translations = Object.create(Translations.prototype);

    return Object.assign(translations, obj, {
      textFlowTargets: obj.textFlowTargets.map((x) => new TextFlowTarget(x))
    });
  }

}

module.exports = Translations;
