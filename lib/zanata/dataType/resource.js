'use strict';

const AbstractResource = require('./abstractResource.js');
const TextFlow = require('./textFlow.js');

class Resource extends AbstractResource {

  constructor(obj) {
    super(obj);
    if (!('textFlows' in obj))
      throw new TypeError('No textFlows provided');
    this.textFlows = obj.textFlows;
  }

  static fromJSON(obj) {
    const resource = Object.create(Resource.prototype);

    return Object.assign(resource, obj, {
      textFlows: obj.textFlows.map((x) => new TextFlow(x))
    });
  }

}

module.exports = Resource;
