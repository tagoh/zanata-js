'use strict';

const TextContainer = require('./textContainer.js');
const ContentState = require('./contentState.js');

class TextFlowTarget extends TextContainer {

  constructor(obj) {
    super(obj);
    if (!('resId' in obj))
      throw new TypeError('missing required properties');
    if (('state' in obj) && Object.keys(ContentState).map((x) => ContentState[x]).indexOf(obj.state) < 0)
      throw new TypeError('Invalid ContentState type');
    if (('extensions' in obj) && !Array.isArray(obj.extensions))
      throw new TypeError('extensions is not an Array');
    this.resId = obj.resId;
    this.state = obj.state;
    this.sourceHash = obj.sourceHash;
    this.extensions = obj.extensions;
    this.revision = obj.revision;
    this.textFlowRevision = obj.textFlowRevision;
    this.description = obj.description;
  }

  static fromJSON(obj) {
    return new this(obj);
  }

}

module.exports = TextFlowTarget;
