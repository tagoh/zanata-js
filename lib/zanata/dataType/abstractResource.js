'use strict';

class AbstractResource {

  constructor(obj) {
    const resourceType = ['FILE', 'DOCUMENT', 'PAGE'];

    if (!('type' in obj) || resourceType.indexOf(obj.type) < 0)
      throw new TypeError('Invalid ResourceType');
    if (!('extensions' in obj) || !Array.isArray(obj.extensions))
      throw new TypeError('extensions is not an Array.');
    this.extensions = obj.extensions;
    this.type = obj.type;
    this.lang = obj.lang;
    this.contentType = obj.contentType;
    this.name = obj.name;
    this.revision = obj.revision;
  }

}

module.exports = AbstractResource;
