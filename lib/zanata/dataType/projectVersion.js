'use strict';

const status = require('./status.js');

class ProjectVersion {

  constructor(obj) {
    if (!('id' in obj) || !('status' in obj) || !('projectType') in obj)
      throw new TypeError('missing required properties');
    if (('links' in obj) && !Array.isArray(obj.links))
      throw new TypeError('links is not an Array');
    if (Object.keys(status).map((x) => status[x]).indexOf(obj.status) < 0)
      throw new TypeError('Invalid StatusType');
    this.id = obj.id;
    this.links = obj.links || [];
    this.status = obj.status;
    this.projectType = obj.projectType;
  }

}

module.exports = ProjectVersion;
