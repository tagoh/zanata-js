'use strict';

class TextContainer {

  constructor(obj) {
    if ('contents' in obj)
      this.contents = obj.contents;
    else if ('content' in obj)
      this.content = obj.content;
    else
      throw new TypeError('no valid arguments');
  }

}

module.exports = TextContainer;
