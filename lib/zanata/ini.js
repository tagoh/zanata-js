'use strict';

const xdg = require('xdg').basedir;
const fs = require('fs');
const path = require('path');
const ini = require('ini');
const URL = require('url');

class Ini {

  constructor() {
    let inifile = path.join(xdg.configHome(), 'zanata.ini');
    try {
      this.resource = ini.parse(fs.readFileSync(inifile, 'utf-8'));
    } catch(e) {
      this.resource = {};
      if (e.code !== 'ENOENT')
        throw e;
    };
  }

  _key(url, name) {
    let parsed = URL.parse(url);
    return parsed.hostname.replace(/\./g, '_') + '.' + name;
  }

  getServerResource(url, name) {
    return this.resource.servers && this.resource.servers[this._key(url, name)] || undefined;
  }

  getUrl(url) {
    return this.getServerResource(url, 'url');
  }

  getUsername(url) {
    return this.getServerResource(url, 'name');
  }

  getKey(url) {
    return this.getServerResource(url, 'key');
  }

  hasResource(url) {
    return this.getUrl(url) != undefined;
  }

}

exports.Ini = Ini;
