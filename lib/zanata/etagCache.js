'use strict';

/**
 * The ETag cache structure definition
 * @exports zanata/etagCache
 *
 * @requires underscore
 */
const _ = require ('underscore');

/**
 * @class
 * @classdesc The ETag cache accessor class
 */
class ETagCache {

  /**
   *
   *
   * @param {object} - the JSON object represented by `JSON.stringify()` this class.
   */
  constructor(obj) {
    if (!(this instanceof ETagCache)) {
      throw new TypeError('Unable to call a class as a function');
    }
    if (obj && obj instanceof Object) {
      _.extend(this, obj);
    }
  }

  /**
   * Set the language
   *
   * @param {string} lang - the language name
   */
  setLanguage(lang) {
    this.lang = lang;
    return this;
  }

  /**
   * Obtain the language name
   *
   * @returns {string} the language name
   */
  getLanguage() {
    return this.lang;
  }

  /**
   * Set the document id
   *
   * @param {string} docId - the document id
   */
  setDocId(docId) {
    this.docId = docId;
    return this;
  }

  /**
   * Obtain the document id
   *
   * @returns {string} the document id
   */
  getDocId() {
    return this.docId;
  }

  /**
   * Set the version id
   *
   * @param {string} versionId - the version id
   */
  setVersionId(versionId) {
    this.versionId = versionId;
    return this;
  }

  /**
   * Obtain the version id
   *
   * @returns {string} the version id
   */
  getVersionId() {
    return this.versionId;
  }

  /**
   * Set the ETag received from the server
   *
   * @param {string} etag - ETag
   */
  setServerETag(etag) {
    this.etag = etag;
    return this;
  }

  /**
   * Obtain the ETag
   *
   * @returns {string} the ETag
   */
  getServerETag() {
    return this.etag;
  }

  /**
   * Set the payload
   *
   * @param {*} data - the payload
   */
  setPayload(data) {
    this.payload = data;
    return this;
  }

  /**
   * Obtain the payload
   *
   * @returns {*} the payload
   */
  getPayload() {
    return this.payload;
  }

}

module.exports = ETagCache;
