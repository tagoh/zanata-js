'use strict';

/**
 * containing all classes to use this library on the client system.
 * @exports ZanataClient
 * @requires zanata/project
 * @requires zanata/fileMapping
 *
 * @namespace
 */
const ZanataClient = {
  /**
   * Project class as defined in {@link module:zanata/project~Project}
   */
  Project: require('./zanata/project.js').Project,
  /**
   * File Mapping Rule Class as defined in {@link module:zanata/fileMapping~FileMappingRule}
   */
  FileMappingRule: require('./zanata/fileMapping.js').FileMappingRule
};

exports.ZanataClient = ZanataClient;
