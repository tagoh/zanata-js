'use strict';

const path = require('path');
const Glob = require('glob').Glob;
const colors = require('colors/safe');
const errmsg = require('./errmsg.js').errmsg;
const _ = require('underscore');


module.exports = function(callback) {
  let cmdpath = path.join(__dirname, '..', 'cmd', '**', '[^_]*\.js');
  let commands = {};
  let g = new Glob(cmdpath, {nodir: true})
      .on('match', (fn) => {
	let cmd = require(fn);
	let o = {};
	o[fn] = cmd;
	_.extend(commands, o);
      })
      .on('error', (err) => {
	g.abort();
	console.error(colors.read("No command directories. zanata-js isn't configured properly."));
	console.error(err);
	callback(colors.red(errmsg(err)));
      })
      .on('end', () => callback(null, commands));
};
