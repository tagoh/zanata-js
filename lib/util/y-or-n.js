'use strict';

const prompt = require('prompt');

module.exports = function(properties, callback) {
  prompt.message = '';
  prompt.start();
  prompt.get(properties, (err, result) => callback(err, result));
};
