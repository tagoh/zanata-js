'use strict';

require('./mbstring.js');

module.exports = {

  table: function(ary, spacing) {
    let widths = [];
    // calculate spacing
    ary.forEach((a) => {
      if ((typeof widths[a.length]) === 'undefined') {
        // initialize results in array
        for (let i = 0; i < a.length; i++)
          if ((typeof widths[i]) === 'undefined')
            widths[i] = 0;
      }
      for (let i = 0; i < a.length; i++) {
        let v = a[i];
        let s = (spacing instanceof Array) ? (spacing[i] || 0) : (spacing || 0);
        let len = s + v.width();

        if (len > widths[i])
          widths[i] = len;
      }
    });
    // output
    return ary.map((a) => {
      let line = '';

      for (let i = 0; i < a.length; i++) {
        line += a[i];
        let len = widths[i] - a[i].width();
        for (let j = 0; j < len; j++)
          line += ' ';
      }
      return line;
    });
  }

};
