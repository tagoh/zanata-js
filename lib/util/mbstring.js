'use strict';

const wcwidth = require('wcwidth');

String.prototype.width = function() {
  return wcwidth(this);
};
