'use strict';

const util = require('util');

module.exports = function(fmt, args) {
  if (args instanceof Object) {
    Object.keys(args).forEach((k) => {
      fmt = fmt.replace(new RegExp('\\\$\{\\s*' + k + '\\s*\}'), args[k]);
    });
  }
  if (fmt.match(/\$/))
    console.warn('Warning: Unreplaced interpolation detected in: ' + fmt + ', ' + util.inspect(args, {depth: null, showHidden: true}));
  return fmt;
};
