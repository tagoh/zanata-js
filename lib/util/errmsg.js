'use strict';

const errno = require('errno');
const util = require('util');
const errmsg = {
  _errmsg: function(err) {
    let ret = '';
    if (err.errno && (typeof err.errno) === 'string' && err.errno.match(/[a-zA-Z]+/)) {
      Object.keys(errno.errno).forEach((k) => {
        if (err.errno === errno.errno[k].code)
          err.errno = k;
      });
    }
    if ((typeof err) === 'string') {
      ret += err;
    } else if (err.request) {
      if (err.code === 'EIO' && err.code) {
        ret += util.format("Unable to reach to %s%s",
                           err.request.options.host,
                           err.request.options.port ? ':' + err.request.options.port : '');
      } else {
        ret = err.toString();
      }
    } else if (err.errno && errno.errno[err.errno])
      ret += errno.errno[err.errno].description;
    else
      ret += 'Unknown error: '+ err;

    if (err.path)
      ret += ': ' + err.path;

    return ret;
  },
  errmsg: function(err) {
    return 'Error: ' + errmsg._errmsg(err);
  },
  warnmsg: function(err) {
    return 'Warning: ' + errmsg._errmsg(err);
  }
};

module.exports = errmsg;
