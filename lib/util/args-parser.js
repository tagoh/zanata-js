'use strict';

const util = require('util');
const path = require('path');
const ary =require('./ary-table.js');

class ArgumentHandler {

  constructor(parent, obj) {
    this.name = obj.name;
    this.description = obj.desc;
    this.object = obj;
    this.parser = new ArgumentParser(parent);
    this.parser.overrideAlias(parent._overrideAlias);
  }

};

class ArgumentParser {

  constructor(parent) {
    if (!(this instanceof ArgumentParser)) {
      return new ArgumentParser();
    }
    this.global = parent ? parent.global : this;
    this.parent = parent;
    this.handlers = {
      options: [],
      commands: {}
    };
    this.values = {};
    this.main_function = () => this.help();
    this._version = '';
    this._usage = '[options...]'
    this._description = '';
    this.nBuiltinCommands = 0;
    this._overrideAlias = false;
    this._ = [];
  }

  version(v) {
    if (v)
      this._version = v;
    else {
      console.log(this._version);
      process.exit(0);
    }

    return this;
  }

  description(msg) {
    this._description = msg;
    return this;
  }

  option(obj) {
    let self = this;
    // conflict check
    self.handlers.options.forEach((o) => {
      if (o.name === obj.name) {
        console.warn('Duplicate option name:');
        console.warn('  original: ' + util.inspect(o, false, null));
        console.warn('  new: ' + util.inspect(obj, false, null));
      } else if (o.alias === obj.alias) {
        if (self._overrideAlias) {
          delete o.alias;
        } else {
          console.warn('Duplicate alias:');
          console.warn('  original: ' + util.inspect(o, false, null));
          console.warn('  new: ' + util.inspect(obj, false, null));
        }
      }
    });
    self.handlers.options.push(obj);
    if (obj.command) {
      let x = self.command(obj);
      if (obj.main)
        x.main(obj.main);
      if (obj.callback && !obj.main)
        x.main(obj.callback);
    }
    return self;
  }

  options(obj) {
    if (obj instanceof Array) {
      let self = this;
      obj.forEach((v) => self.option(v));
    }
    return this;
  }

  command(obj) {
    if (obj.builtin)
      this.nBuiltinCommands++;
    let cmd = new ArgumentHandler(this, obj);
    this.handlers.commands[obj.name] = cmd;
    return cmd.parser;
  }

  commands(obj) {
    if (obj instanceof Array) {
      let self = this;
      obj.forEach((v) => {
        self
          .command(v)
          .usage(v.usage)
          .description(v.desc)
          .options(v.options)
          .example(v.examples)
          .main(v.main)
          .commands(v.commands)
          .end()
      });
    }
    return this;
  }

  example(obj) {
    if (obj instanceof Array) {
      this._example = obj.join('\n');
    } else {
      this._example = obj;
    }
    return this;
  }

  main(callback) {
    if ((typeof callback) === 'function')
      this.main_function = callback;
    return this;
  }

  end() {
    if (this.parent)
      return this.parent;
    return null;
  }

  overrideAlias(f) {
    this._overrideAlias = (f === true);
    return this;
  }

  parse(args) {
    let action = false;
    let self = this;
    let lastOpt = null;
    if (!args)
      args = process.argv;
    if (!this.parent)
      args = args.slice(2);

    this.option({name: 'help', alias: 'h', desc: 'Display this help', builtin: true, callback: function(x) {x.help()}});
    this.option({name: 'version', desc: 'Display this version', builtin: true, callback: function(x) {x.global.version()}});

    let xargs = this.normalize(args);
    let nosub = false;
    for (let i = 0; i < xargs.length; i++) {
      let v = xargs[i];
      if (v === '--') {
        // option terminator
        console.log('XXX: terminated');
      } else {
        let processed = false;
        this.handlers.options.forEach((x) => {
          if (v instanceof Object) {
            if ((v.alias && x.alias === v.alias) ||
                (v.name && x.name === v.name)) {
              if (x.argument && !v.value) {
                if ((typeof xargs[i + 1]) === 'string') {
                  v.value = xargs[i + 1];
                  i++;
                } else {
                  if (x.argument[0] !== '[') {
                    if (v.alias)
                      console.error(util.format("Error: -%s requires a value", x.alias));
                    else
                      console.error(util.format("Error: --%s requires a value", x.name));
                    process.exit(1);
                  }
                }
              }
              let f = (err) => {
                if (err) {
                  console.error(util.format("Error: %s", err));
                  process.exit(1);
                } else {
                  if (x.value_type === 'count') {
                    if (self.values[x.name])
                      self.values[x.name]++;
                    else
                      self.values[x.name] = 1;
                  } else if (x.value_type === 'boolean') {
                    let val = v.value && v.value.toLowerCase();
                    if (val === 'f' || val === '0' || val === 'false' || val === 'nil' || val === 'no' || val === 'n')
                      self.values[x.name] = false;
                    else if (val === 't' || val === '1' || val === 'true' || val === 'yes' || val === 'y')
                      self.values[x.name] = true;
                    else {
                      console.error('Invalid boolean value');
                      process.exit(1);
                    }
                  } else if (x.argument) {
                    if (x.argument[0] === '<' && x.argument.slice(-1) === '>' && x.argument.indexOf('|') > 0) {
                      // argument value has to be one of them
                      let selections = x.argument.slice(1, -1).split('|').map((v) => v.toLowerCase());
                      if (selections.indexOf(v.value.toLowerCase()) < 0) {
                        console.error(util.format("%s is invalid value for --%s. it has to be one of: %s",
                                                  v.value, x.name, selections.join(', ')));
                        process.exit(1);
                      }
                      self.values[x.name] = v.value;
                    } else if (x.argument[0] === '<' && x.argument.slice(-1) === '>' && x.argument.indexOf(',') > 0) {
                      // multiple values
                      let selections = x.argument.slice(1, -1).split(',').map((v) => v.toLowerCase());
                      let origSel = {};

                      x.argument.slice(1, -1).split(',').forEach((v) => origSel[v.toLowerCase()] = v);
                      self.values[x.name] = [];
                      v.value.split(',').forEach((s) => {
                        if (selections.indexOf(s) < 0) {
                          console.error(util.format("%s is invalid value for --%s. it has to be anything from: %s",
                                                    s, x.name, selections.join(', ')));
                          process.exit(1);
                        }
                        self.values[x.name].push(origSel[s]);
                      });
                    } else
                      self.values[x.name] = v.value;
                  } else
                    self.values[x.name] = true;
                }
              };
              if (x.callback)
                x.callback(self, v, f);
              else
                f(null);
              processed = true;
            }
          }
        });
        if (!processed) {
          if ((typeof v) === 'string') {
            if (nosub) {
              self._.push(v);
            } else {
              let cmds = Object.keys(self.handlers.commands);
              action = self.run(v, xargs.slice(i + 1), (err) => {
                if ((cmds.length - self.nBuiltinCommands) <= 0) {
                  // no sub-commands. so call main function after all of options processed.
                  nosub = true;
                  self._.push(v);
                } else {
                  console.error(err);
                  process.exit(1);
                }
              });
              if (action)
                break;
            }
          } else {
            console.error(util.format("Error: Invalid option: %s", v.alias ? '-' + v.alias : '--' + v.name));
            process.exit(1);
          }
        }
      }
    }
    if (!action && this.main_function) {
      this.main_function(this, this._);
    }
  }

  normalize(args) {
    if (!(args instanceof Array))
      return args;
    let ret = [];

    args.forEach((v) => {
      if (v.length > 2 && v[0] == '-' && v[1] == '-') {
        let i = v.indexOf('=');
        // separate an option and value
        if (i > 0) {
          ret.push({name: v.slice(2, i), value: v.slice(i + 1)});
        } else {
          ret.push({name: v.slice(2)});
        }
      } else if (v.length > 1 && v[0] == '-') {
        v.slice(1).split('').forEach((x) => ret.push({alias: x}));
      } else {
        ret.push(v);
      }
    });

    return ret;
  }

  run(cmd, args, callback) {
    let action = false;
    let self = this;
    Object.keys(this.handlers.commands).forEach((key) => {
      if (key === cmd) {
        let v = self.handlers.commands[key];
        v.parser.parse(args);
        action = true;
      }
    });
    if (!action) {
      let err = 'Error: No such command available: ' + cmd;
      if ((typeof callback) === 'function') {
        callback(err);
      } else {
        console.error(err);
        process.exit(1);
      }
    }
    return action;
  }

  usage(msg, callback) {
    if ((typeof callback) === 'function') {
      let u = this._usage;

      if (msg)
        u = this._usage.replace(/ ?\[.*\]/, '').replace(/ <.*>/, '');
      if (this.parent)
        this.parent.usage(u + ' ' + msg, callback);
      else
        callback(util.format('Usage: %s %s %s', path.basename(process.argv[1]), u, msg));
    } else if (msg) {
      this._usage = msg;
    }
    return this;
  }

  help() {
    let self = this;
    let msg = [];

    this.usage('', (m) => msg.push(m));
    if (this._description) {
      msg.push('');
      msg.push(this._description);
    }
    msg.push('');
    msg.push('Options:');
    let arydata = [];
    this.handlers.options.forEach((v) => {
      let a = ['']; // insert an empty string for indent
      let m = '';
      if (v.alias) {
        m += '-' + v.alias;
        if (v.name)
          m += ', ';
      }
      if (v.name)
        m += '--' + v.name;
      if (v.argument)
        m += '=' + v.argument;
      a.push(m);
      a.push(v.desc);
      arydata.push(a);
    });
    let res = ary.table(arydata, [2, 4]);
    msg = msg.concat(res);
    if (this._example) {
      msg.push('');
      msg.push(this._example);
    }
    let keys = Object.keys(this.handlers.commands);
    if (keys.length > this.nBuiltinCommands) {
      msg.push('');
      msg.push('Available commands:');
      arydata = [];
      keys.forEach((k) => {
        let a = ['']; // insert an empty string for indent
        let v = self.handlers.commands[k];
        if (!v.object.builtin) {
          a.push(v.name);
          a.push(v.description);
          arydata.push(a);
        }
      });
      res = ary.table(arydata, [2, 4]);
      msg = msg.concat(res);
      msg.push('');
      msg.push(util.format('To show more details for each commands, try %s <command> --help', path.basename(process.argv[1])));
    }
    console.error(msg.join('\n'));
    process.exit(0);
  }

}

exports.ArgumentHandler = ArgumentHandler;
exports.ArgumentParser = ArgumentParser;
exports = module.exports = new ArgumentParser();
