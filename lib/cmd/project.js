'use strict';

const util = require('util');
const Project = require('../zanata/project.js').Project;
const ary = require('../util/ary-table.js');

module.exports = {
  name: 'project',
  desc: 'Project Operations on Zanata',
  usage: 'project [options] <command>',
  callback: function(self, args) {
    self.help();
  },
  options: [
    {name: 'config',
     alias: 'C',
     desc: 'The place of zanata.xml to read',
     argument: '<FILE>',
    }
  ],
  commands: [
    {
      name: 'list',
      desc: 'Show all projects available on the server',
      usage: 'list [options]',
      options: [
	{name: 'column',
	 alias: 'c',
	 desc: 'Columns to show',
	 argument: '<id,name,defaultType,status,all>'
	}
      ],
      main: function(self, args) {
	let target = ['id', 'name', 'defaultType', 'status']; // links
	if (self.values.column) {
	  if (self.values.column.indexOf('all') < 0)
	    target = self.values.column;
	} else {
	  target = ['id', 'name', 'status']; // default values
	}
	new Project({url: self.global.values.url,
		     username: self.global.values.user,
		     'api-key': self.global.values['api-key'],
		     'project-config': self.parent.values.config})
          .on('fail', (e) => {
            console.error(e);
          })
          .on('data_list', (d) => {
	    let arydata = [];
	    // header
	    arydata.push(Object.keys(d[0]).map((k) => k));
	    // separator
	    arydata.push(Object.keys(d[0]).map((k) => Array(k.length+3).join('-')));
	    d.forEach(function(x) {
	      arydata.push(Object.keys(x).map((k) => x[k]));
	    });
	    console.log(ary.table(arydata, 2).join('\n'));
          })
          .list(target);
      }
    }, // end of 'list'
    {
      name: 'create',
      desc: 'Create a project on the server',
      usage: 'create [options] <project id>',
      options: [
	{name: 'description',
	 alias: 'd',
	 desc: 'Description for the project',
	 argument: '<DESCRIPTION>'
	},
	{name: 'project',
	 alias: 'p',
	 desc: 'Project name to create',
	 argument: '<NAME>',
	},
	{name: 'project-type',
	 alias: 't',
	 desc: 'Default project type. Versions under this project that do not specify a project type will use this default.',
	 argument: '<File|Gettext|Podir|Properties|Utf8Properties|Xliff|Xml>'
	},
      ],
      examples: [
	'The default project type must be one of:'
      ].concat(ary.table([
	['', 'File', 'For plain text, Libre Office, InDesign, HTML, Subtitles etc.'],
	['', 'Gettext', 'For Gettext software strings.'],
	['', 'Podir', 'For Publican/Docbook strings.'],
	['', 'Properties', 'For Java properties files.'],
	['', 'Utf8Properties', 'For UTF8-encodded Java properties.'],
	['', 'Xliff', 'For supported XLIFF files'],
	['', 'Xml', 'For XML from the Zanata REST API.']], [2, 4])),
      main: function(self, args) {
	if (args.length < 1) {
	  console.error('Project id is required');
	  process.exit(1);
	}
	new Project({url: self.global.values.url,
		     username: self.global.values.user,
		     'api-key': self.global.values['api-key'],
		     'project-config': self.parent.values.config})
          .on('fail', function(e) {
            console.error(e);
          })
          .on('data_create', function(d) {
            console.log(d);
          })
	  .create({project: args[0],
                   'project-type': self.values['project-type'],
                   'project-name': self.values.project,
                   description: self.values.description});
      }
    }, // end of 'create'
    {
      name: 'info',
      desc: 'Show the project information',
      usage: 'info [options] <project id> ...',
      options: [
	{name: 'show-locales',
	 alias: 'l',
	 desc: 'Show available languages for translating',
	}
      ],
      main: function(self, args) {
	if (args.length === 0) {
	  console.error('No project ids');
	  process.exit(1);
	}
	let p = new Project({url: self.global.values.url,
			     username: self.global.values.user,
			     'api-key': self.global.values['api-key'],
			     'project-config': self.parent.values.config})
            .on('fail', function(e) {
              console.error(e);
              process.exit(1);
            })
            .on('data_info', function(d) {
	      // deal with them separately later
	      let ignoreProps = ['versions', 'locales'];
	      let arydata = Object.keys(d)
		  .filter(function(v) {return ignoreProps.indexOf(v) < 0;})
		  .map(function(k) {
		    return [k, d[k]];
		  });
	      // versions
	      if (d.versions) {
		arydata.push(['Versions']);
		// header
		let a = [Object.keys(d.versions[0]).map(function(k) {
		  return k;
		})];
		d.versions.forEach(function(o) {
		  a.push(Object.keys(o).map(function(k) {
		    return o[k];
		  }));
		});
		arydata = arydata.concat(ary.table(a, 2).map(function(v) {return ['', v];}));
	      }
	      // locales
	      if (d.locales) {
		arydata.push(['Translatable Languages']);
		d.locales.forEach(function(o) {
		  arydata.push(['', o.displayName + ' [' + o.localeId + ']']);
		});
	      }
	      let res = ary.table(arydata, [2, 4]);
	      console.log(res.join('\n'));
              console.log('');
	    });
	args.forEach(function(arg) {
	  p.info(arg, self.values['show-locales']);
        });
      }
    }, // end of 'info'
    {
      name: 'create-version',
      desc: 'Create a version in the project',
      usage: 'create-version [options] <project version id> ...',
      options: [
	{name: 'project',
	 alias: 'p',
	 desc: 'Project ID to show a version information',
	 argument: '<PROJECT ID>',
        },
	{name: 'project-type',
	 alias: 't',
	 desc: 'Default project type. Versions under this project that do not specify a project type will use this default.',
	 argument: '<File|Gettext|Podir|Properties|Utf8Properties|Xliff|Xml>'
	}
      ],
      examples: [
	'The default project type must be one of:'
      ].concat(ary.table([
	['', 'File', 'For plain text, Libre Office, InDesign, HTML, Subtitles etc.'],
	['', 'Gettext', 'For Gettext software strings.'],
	['', 'Podir', 'For Publican/Docbook strings.'],
	['', 'Properties', 'For Java properties files.'],
	['', 'Utf8Properties', 'For UTF8-encodded Java properties.'],
	['', 'Xliff', 'For supported XLIFF files'],
	['', 'Xml', 'For XML from the Zanata REST API.']], [2, 4])),
      main: function(self, args) {
        if (args.length === 0) {
          console.error('No project version ID');
          process.exit(1);
        }
        let p = new Project({url: self.global.values.url,
			     username: self.global.values.user,
			     'api-key': self.global.values['api-key'],
			     'project-config': self.parent.values.config})
            .on('fail', function(e) {
              console.error(e);
              process.exit(1);
            })
            .on('data_create', console.log);
	args.forEach(function(arg) {
	  p.createVersion(self.values.project, arg,
                          {'project-type': self.values['project-type']});
	});
      }
    }, // end of 'create-version'
    {
      name: 'version-info',
      desc: 'Show the project version information',
      usage: 'version-info [options] <project version id> ...',
      options: [
	{name: 'project',
	 alias: 'p',
	 desc: 'Project ID to show a version information',
	 argument: '<PROJECT ID>',
	},
	{name: 'show-locales',
	 alias: 'l',
	 desc: 'Show available languages for translating on the version',
	}
      ],
      main: function(self, args) {
	if (args.length === 0) {
	  console.error('No project version ids');
	  process.exit(1);
	}
	let p = new Project({url: self.global.values.url,
			     username: self.global.values.user,
			     'api-key': self.global.values['api-key'],
			     'project-config': self.parent.values.config})
            .on('fail', function(e) {
              console.error(e);
              process.exit(1);
            })
            .on('data_version_info', function(d) {
	      let arydata = [];
	      Object.keys(d).forEach(function(k) {
		if (k !== 'locales')
		  arydata.push([k, d[k]]);
	      });
	      // locales
	      if (d.locales) {
		arydata.push(['Translatable Languages']);
		d.locales.forEach(function(o) {
		  arydata.push(['', o.displayName + ' [' + o.localeId + ']']);
		});
	      }
	      console.log(ary.table(arydata, [2, 4]).join('\n'));
              console.log('');
	    });
	args.forEach(function(arg) {
	  p.versionInfo(self.values.project, arg, self.values['show-locales']);
	});
      }
    }, // end of 'version-info'
    {
      name: 'pull',
      desc: 'Pull translated text from the server',
      usage: 'pull [options]',
      options: [
	{name: 'project',
	 alias: 'p',
	 desc: 'Project ID to pull documents from the server',
	 argument: '<PROJECT ID>',
	},
	{name: 'project-version',
	 alias: 'V',
	 desc: 'Project version to pull documents from the server',
	 argument: '<VERSION>',
	},
        {name: 'project-type',
         alias: 't',
         desc: 'Project type pulling documents from the server',
	 argument: '<File|Gettext|Podir|Properties|Utf8Properties|Xliff|Xml>'
	},
	{name: 'pull-type',
	 alias: 'T',
	 desc: 'Type of pull to perform from the server.',
	 argument: '<source|trans|both>',
	},
	{name: 'locales',
	 alias: 'l',
	 desc: 'Locales to pull translations from the server.',
	 argument: '<LOCALE...>',
	 callback: function(self, arg, cb) {
	   arg.value = arg.value.split(',');
	   cb(null);
	 }
	},
	{name: 'create-skeletons',
	 alias: 'k',
	 desc: 'Create skeleton entries for strings/files which have not been translated yet.',
	},
	{name: 'pot-dir',
	 desc: 'The place to store the POT file',
         argument: '<DIR>'
	},
	{name: 'po-dir',
	 desc: 'The place to store the PO files',
         argument: '<DIR>'
	},
        {name: 'force',
         alias: 'f',
         desc: 'Force to transfer the data'
        },
	{name: 'verbose',
	 alias: 'v',
	 desc: 'Show more progress messages verbosely',
         value_type: 'count'
	},
        {name: 'dry-run',
         desc: 'Do not perform any operations from the server'
        }
      ],
      examples: [
	'the pull type must be one of:'
      ].concat(ary.table([
	['', 'source', 'source document only'],
	['', 'trans', 'translation document only (default)'],
	['', 'both', 'both source and translation documents']
      ], [2, 4])),
      main: require('./_pull.js')
    }, // end of 'pull'
    {
      name: 'push',
      desc: 'Push translated text to the server',
      usage: 'push [options]',
      options: [
	{name: 'project',
	 alias: 'p',
	 desc: 'Project ID to push documents to the server',
	 argument: '<PROJECT ID>',
	},
	{name: 'project-version',
	 alias: 'V',
	 desc: 'Project version to push documents to the server',
	 argument: '<VERSION>',
	},
        {name: 'project-type',
         alias: 't',
         desc: 'Project type pulling documents from the server',
	 argument: '<File|Gettext|Podir|Properties|Utf8Properties|Xliff|Xml>'
	},
	{name: 'push-type',
	 alias: 'T',
	 desc: 'Type of push to perform to the server.',
	 argument: '<source|trans|both>',
	},
	{name: 'locales',
	 alias: 'l',
	 desc: 'Locales to push translations to the server.',
	 argument: '<LOCALE...>',
	 callback: function(self, arg, cb) {
	   arg.value = arg.value.split(',');
	   cb(null);
	 }
	},
	{name: 'pot-dir',
	 desc: 'The place to read the POT file',
         argument: '<DIR>'
	},
	{name: 'po-dir',
	 desc: 'The place to read the PO files',
         argument: '<DIR>'
	},
	{name: 'copy-trans',
	 desc: 'Copy latest translations from equivalent messages/documents in the database',
	 argument: '<BOOLEAN>',
	 value_type: 'boolean',
	},
	{name: 'dry-run',
	 desc: 'Do not perform any operations to the server',
	},
	{name: 'verbose',
	 alias: 'v',
	 desc: 'Show more progress messages verbosely',
         value_type: 'count'
	}
      ],
      examples: [
	'the push type must be one of:'
      ].concat(ary.table([
	['', 'source', 'source document only'],
	['', 'trans', 'translation document only (default)'],
	['', 'both', 'both source and translation documents']
      ], [2, 4])),
      main: require('./_push.js')
    }, // end of 'push'
    {
      name: 'stats',
      desc: 'Document statistics',
      usage: 'stats [options]',
      options: [
        {name: 'project',
         alias: 'p',
         desc: 'Project ID to get statistics from the server',
         argument: '<PROJECT ID>',
        },
        {name: 'project-version',
         alias: 'V',
         desc: 'Project version to get statistics from the server',
         argument: '<VERSION>',
        },
        {name: 'doc-id',
         alias: 'D',
         desc: 'Document ID to get statistics from the server',
         argument: '<DOC ID>',
        },
        {name: 'detail',
         alias: 'd',
         desc: 'Whether to include detailed statistics',
        },
        {name: 'word',
         alias: 'w',
         desc: 'Whether to include word-level statistics',
        },
        {name: 'locales',
         alias: 'l',
         desc: 'What locales to get statistics for',
         argument: '<LOCALE...>',
	 callback: function(self, arg, cb) {
	   arg.value = arg.value.split(',');
	   cb(null);
	 }
        },
	{name: 'verbose',
	 alias: 'v',
	 desc: 'Show more progress messages verbosely',
         value_type: 'count'
	}
      ],
      examples: [
      ],
      main: function(self, args) {
        let verbose = self.values['verbose'] || 0;
        let proc = (h, id, d, cb) => {
          let prevLocale = '';
          let result = [[h, id]].concat(d.stats.map((o) => {
            let res = [prevLocale === o.locale ? '' : o.locale,
                       util.format("%s / %s %s translated",
                                   o.translated,
                                   o.total,
                                   o.unit.toLowerCase() + '(s)'),
                       '(' + Number((o.translated * 100 / o.total).toFixed(2)) + '%)'];
            prevLocale = o.locale;
            return res;
          }));
          cb(result);
        };
        let opts = {detail: self.values.detail,
                    word: self.values.word,
                    locales: self.values.locales};

        if (self.values['doc-id']) {
	  new Project({url: self.global.values.url,
		       username: self.global.values.user,
		       'api-key': self.global.values['api-key'],
		       'project-config': self.parent.values.config})
            .on('fail', (e) => {
              if (verbose > 1)
                console.error(e.stack.red);
              else
                console.error(e.toString().red);
              process.exit(1);
            })
            .on('data_stats', (d) => {
              proc('Doc', d.id, d, (res) => console.log(ary.table(res, 2).join('\n')));
            })
            .stats(self.values.project,
                   self.values['project-version'],
                   self.values['doc-id'],
                   opts);
        } else {
	  new Project({url: self.global.values.url,
		       username: self.global.values.user,
		       'api-key': self.global.values['api-key'],
		       'project-config': self.parent.values.config})
            .on('fail', (e) => {
              if (verbose > 1)
                console.error(e.stack.red);
              else
                console.error(e.toString().red);
              process.exit(1);
            })
            .on('data_stats', (d) => {
              proc('Version', d.id, d, (res) => {
                console.log(ary.table(res, 2).join('\n'));
                if (d.detailedStats) {
                  console.log('');
                  d.detailedStats.forEach((o) => {
                    proc('Doc', o.id, o, (r) => console.log(ary.table(r, 2).join('\n')));
                  });
                }
              });
            })
            .stats(self.values.project,
                   self.values['project-version'],
                   opts);
        }
      }
    }, // end of 'stats'
  ]
};
