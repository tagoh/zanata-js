'use strict';

const Project = require('../zanata/project.js').Project;

module.exports = function(self, args) {
  let verbose = self.values.verbose || 0;
  let p = new Project({url: self.global.values.url,
                       username: self.global.values.user,
                       'api-key': self.global.values['api-key'],
                       'project-config': self.parent.values.config,
                       'project-type': self.values['project-type']})
      .on('fail', (e) => {
        if (verbose > 1)
          console.error(e.stack.red);
        else
          console.error(e.toString().red);
      })
      .on('data_push', (fn) => {
        if (verbose > 0)
          console.log('Uploaded ' + fn);
      })
      .on('end_push', (r) => {
        if (self.values['dry-run']) {
          console.log('Those file(s) will be pushed onto the server');
          console.log('  ' + r.map((o) => o.file).join(', '));
        } else {
          if (verbose > 0)
            console.log('Successfully completed.'.green);
        }
      })
      .push({project: self.values.project,
             version: self.values['project-version'],
             pushType: self.values['push-type'],
             locales: self.values['locales'],
             verbose: self.values['verbose'],
             potdir: self.values['pot-dir'],
             podir: self.values['po-dir'],
             dryrun: self.values['dry-run'],
             copyTrans: self.values['copy-trans']
            });
};
