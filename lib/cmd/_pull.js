'use strict';

const fs = require('fs');
const path = require('path');
const colors = require('colors');
const Project = require('../zanata/project.js').Project;
const fmr = require('../zanata/fileMapping.js').FileMappingRule;

module.exports = function(self, args) {
  let verbose = self.values['verbose'] || 0;
  let p = new Project({url: self.global.values.url,
                       username: self.global.values.user,
                       'api-key': self.global.values['api-key'],
                       'project-config': self.parent.values.config})
      .on('fail', (e) => {
        if (verbose > 1)
          console.error(e.stack.red);
        else
          console.error(e.toString().red);
      })
      .on('warning', (e) => {
        if (verbose > 1)
          console.warn(e.yellow);
      })
      .on('data_pull', (d) => {
        if (!self.values['dry-run']) {
          let potdir = self.values['pot-dir'] || './po';
          let podir = self.values['po-dir'] || './po';
          let projectType = self.values['project-type'] || p.config.get('project-type');
          let fm = new fmr(p.config.get('rules'));
          let file;

          if (d.type === 'pot') {
            file = path.join(potdir, d.name + '.pot');
          } else {
            file = fm.getPath(projectType, {path: podir, locale: d.locale, filename: d.name, extension: d.type, source: path.join(potdir, d.name + '.pot')});
          }
          if (verbose > 0)
            process.stdout.write("Downloading " + (d.type === 'pot' ? 'POT' : 'PO') + " into " + file + "...");
          fs.writeFile(file, d.data, (e) => {
            if (verbose > 0) {
              if (e) {
                process.stdout.write("ERR!!\n".red);
              } else {
                process.stdout.write("DONE.\n".green);
              }
            }
          });
        }
      })
      .on('end_pull', (r) => {
        if (self.values['dry-run']) {
          let fm = new fmr(p.config.get('rules'));
          let filtered = r.filter((o) => o.pulled);

          if (filtered.length > 0) {
            console.log('Those file(s) will be pulled from the server');
            console.log('  ' + r.map((o) => {
              if (o.locale)
                return fm.getPath(self.values['project-type'] || p.config.get('project-type'),
                                  {path: self.values['po-dir'] || './po',
                                   locale: o.locale,
                                   filename: o.name,
                                   extension: o.type});
              else
                return path.join(self.values['pot-dir'] || './po', o.name + '.pot');
            }).join(', '));
          } else {
            console.log('No files will be pulled from the server');
          }
        } else {
          if (verbose > 0)
            console.log('Successfully completed.'.green);
        }
      })
      .pull({project: self.values.project,
             version: self.values['project-version'],
             pullType: self.values['pull-type'],
             locales: self.values['locales'],
             verbose: self.values['verbose'],
             skeletons: self.values['create-skeletons'],
             potdir: self.values['pot-dir'],
             podir: self.values['po-dir'],
             force: self.values['force']});
};
