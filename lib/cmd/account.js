'use strict';

const ar = require('../zanata/api/accountResource.js').AccountResource;
const util = require('util');
const errmsg = require('../util/errmsg.js').errmsg;

module.exports = {
  name: 'account',
  desc: 'Account Operations on Zanata',
  usage: 'account [options] <command>',
  callback: function(self, args) {
    self.help();
  },
  options: [],
  commands: [
    {
      name: 'info',
      desc: 'Show the account information',
      usage: 'info <user id>',
      options: [],
      main: function(self, args) {
        if (args.length == 0) {
          console.error('No user ids');
          process.exit(1);
        }
        let a = new ar(self.global.values.url,
                       {user: self.global.values.user,
                        'api-key': self.global.values['api-key']});
        args.forEach(function(arg) {
          a.get(arg, function(err, data) {
            if (err) {
              console.error(errmsg(err));
            } else {
              console.log(data);
            }
          });
        });
      }
    }, // end of 'info'
  ]
};
