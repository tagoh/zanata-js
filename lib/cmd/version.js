'use strict';

const vr = require('../zanata/api/versionResource.js').VersionResource;
const ary = require('../util/ary-table.js');
const pkg = require('../../package.json');
const errmsg = require('../util/errmsg.js').errmsg;

module.exports = {
  name: 'version',
  desc: 'Show the server version and the client version',
  usage: 'version',
  main: function(self, args) {
    new vr(self.global.values.url)
      .get()
      .then((d) => {
        let x = [['', 'Client version', pkg.version]];
        let data = d.object || d.json;
        Object.keys(data).forEach((k) => {
          x.push(['', k, data[k]]);
        });
        let res = ary.table(x, [2, 4]);
        console.log(res.join('\n'));
      })
      .catch((e) => {
        console.error(errmsg(e));
      });
  }
};
